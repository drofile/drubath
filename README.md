Drubath
=======================================

Documentation:

Features
----

Drubath is essentially a base theme from which custom site themes can be built upon. It includes a number of default files & plugins to help speed up theme development. 
It is entirely independent from Drofile, and can be dropped into any Drupal installation.

Included with the theme are:

* Custom Foundation 5 base setup. Used purely for reset & layout options (no styling has been kept)
* Animate.css for smooth CSS3 animations 
* Velocity.js for more efficient & smoother jQuery animations
* Two basic panels layouts (1col & 2cols)
* Breakpoint settings for the ‘picture’ module

Installation: 
----
It is recommended to rename the important theme files & functions with the name of the project (e.g. drubath > _projectname_). 
To do that, do a find/replace on all theme files and search for ‘drubath’. 
The following files will need renaming once that has been complete:  [(1)](#1-rename-dirs-and-files)
* drubath [dir]
* drubath.info
* js/drubath.fronts.js
* js/drubath.global.js
* js/drubath.plugins.js
* js/drubath.emmsg.js
* js/drubath.overlay.js

Once that is all done, you should ‘enable & set default’ the theme and begin development :)


Options
----
You can setup a number of optional plugins & settings on the theme settings page (/admin/appearance/settings/yourtheme), including:

* Swipebox\*

  This enables the ‘swipebox’ jquery plugin for galleries
  
* Slick\*

  This enables the ‘slick.js’ jquery plugin for carousels etc

* MatchHeight\*

  This enables the ‘matchHeight.js’ plugin, which is a helper plugin to equalise sibling elements.
* Overlay

  This enables the custom overlay js/css to have customised overlays on the site. 
  Usage can be found in ‘drubath.overlay.js’

* Copyright text

  This is used to output the correct year along with any copyright text required on the site. The output can be found on both the ‘page.tpl.php’ & ‘page--panel.tpl.php’ templates


*For usage, options & customisation of the jquery modules, the ‘/js/yourtheme.plugins.js’ file contains the default configuration & notes for all plugins.


---------------------------------------
#### (1) RENAME dirs and files

Go to themes dir (docroot/sites/all/themes) and run:

##### This will rename all files from drubath to _projectname_ 
```
find . -type f | while read -r file; do
    mv "$file" "${file//drubath/projectname}"
done
```
##### This will rename all dirs from drubath to projectname
```
find . -type d | while read -r file; do
    mv "$file" "${file//drubath/projectname}"
done
```
##### This will rename files content from drubath to projectname

cd into theme dir, and then:
`find . -name '*.*' -type f -exec sed -i -e 's/drubath/projectname/g' -- {} +`


After running these commands, check if everything looks as expected (ie: no references for 'drubath' are found in file/dir names or in files contents). If thats the case you are ready to enable and set your theme as default.
